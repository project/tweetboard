ELMS: Tweetboard - Twitter Integration via the tweetboard js app.
Copyright (C) 2009-2010  The Pennsylvania State University

Bryan Ollendyke
bto108@psu.edu

Keith D. Bailey
kdb163@psu.edu

12 Borland
University Park, PA 16802

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


Use the following to get a faster approval for your site when using this module!
Code: DRUPALINVITES01
URL: http://tweetboard.com/alpha/invites/DRUPALINVITES01/

Install:
1. Make sure you get an invite at tweetboard.com
2. Turn it on and go to the configuration page admin/settings/tweetboard.  Enter your twitter username
3. Go to the admin/user/permissions page to give people the ability to view the tweetboard based on role

Compatability:
This module should be compatable with any / all browser that Tweetboard is.