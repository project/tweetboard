<?php
//ELMS: Tweetboard - Twitter Integration via the tweetboard js app.
//Copyright (C) 2009-2010  The Pennsylvania State University
//
//Bryan Ollendyke
//bto108@psu.edu
//
//Keith D. Bailey
//kdb163@psu.edu
//
//12 Borland
//University Park, PA 16802
//
//This program is free software; you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation; either version 2 of the License, or
//(at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License along
//with this program; if not, write to the Free Software Foundation, Inc.,
//51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

/**
 * @file
 * Simple Integration of the tweetboard web app.
 *
 *
 */

/**
 * Implementation of hook_permission().
 */
function tweetboard_permission() {
  return array(
    'view tweetboard object' =>  array(
      'title' => t('View Tweetboard'),
      'description' => t('Give the ability to view / use the tweetboard widget.'),
    ),
  );
}

/**
 * Implementation of hook_menu().
 */
function tweetboard_menu() {
  $items = array();
  $items['admin/config/user-interface/tweetboard'] = array(
    'title' => 'Tweetboard',
    'description' => "Set the user account and you're off and running",
    'page callback' => 'drupal_get_form',
    'page arguments' => array('tweetboard_settings'),
    'access arguments' => array('administer site configuration'),
  );
  return $items;
}

/**
 * Implementation of hook_settings().
 */
function tweetboard_settings() {
  $form['tweetboard_user'] = array(
    '#title' => t('User name'),
    '#type' => 'textfield',
    '#size' => 120,
    '#maxlength' => 200,
    '#default_value' => check_plain(variable_get('tweetboard_user', '')),
    '#description' => t('You need to have your twitter username / website address approved by tweetboard.com before tweetboard will work properly on your site.'),
  );
  $form = system_settings_form($form);
  return $form;
}

/**
 * Implementation of hook_page_alter().
 */
function tweetboard_page_alter(&$page) {
  if (user_access('view tweetboard object') ) {
    if (arg(0) != 'admin' && arg(0) != 'emvideo' && variable_get('tweetboard_user', '') != '') {
      $page['page_bottom']['tweetboard']= array(
      '#type' => 'markup',
      '#markup' => '<script src="http://tweetboard.com/'. check_plain(variable_get('tweetboard_user', '')) .'/tb.js" type="text/javascript"></script>');
    }
  }
}
